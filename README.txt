CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Configuration
 * Maintainers


INTRODUCTION
------------

Permission Set module to customize your own Permission set page.


CONFIGURATION
-------------

/admin/config/configure-permission-set
 * Select the Permissions and change the permission label if require.

/admin/config/permission_group
 * Create Permission groups to bind multiple permissions in single set
 of permission.

/admin/config/permission-set
 * Customized Permission set to assign permissions to roles.


MAINTAINERS
-----------

Current maintainers:
 * Praveen Paliya (ppaliya) - https://www.drupal.org/u/ppaliya
 * Surya Prakash Gangwar (gangwarsurya) - https://www.drupal.org/u/gangwarsurya
