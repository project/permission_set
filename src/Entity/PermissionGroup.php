<?php

namespace Drupal\permission_set\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Permission group entity.
 *
 * @ConfigEntityType(
 *   id = "permission_group",
 *   label = @Translation("Permission group"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\permission_set\PermissionGroupListBuilder",
 *     "form" = {
 *       "add" = "Drupal\permission_set\Form\PermissionGroupForm",
 *       "edit" = "Drupal\permission_set\Form\PermissionGroupForm",
 *       "delete" = "Drupal\permission_set\Form\PermissionGroupDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\permission_set\PermissionGroupHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "permission_group",
 *   admin_permission = "manage permission groups",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "permissions" = "permissions",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/people/permission_group/{permission_group}",
 *     "add-form" = "/admin/config/people/permission_group/add",
 *     "edit-form" = "/admin/config/people/permission_group/{permission_group}/edit",
 *     "delete-form" = "/admin/config/people/permission_group/{permission_group}/delete",
 *     "collection" = "/admin/config/people/permission_group"
 *   }
 * )
 */
class PermissionGroup extends ConfigEntityBase implements PermissionGroupInterface {

  /**
   * The Permission group ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Permission group label.
   *
   * @var string
   */
  protected $label;
  /**
   * The Permission label.
   *
   * @var string
   */
  protected $permissions;

}
